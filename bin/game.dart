import 'Character.dart';
import 'Monster.dart';
import 'Player.dart';
import 'dart:io';
import 'dart:math';

class Game {
  bool win = false;
  bool walkSucc = false;
  bool end = true;
  Player p1 = new Player(" ");
  Monster m1 = new Monster(" ");

  Game() {
    print("-*-*-*- Welcome to Game -*-*-*-");
    print("Are you ready to advanture in Epic tale ? (y/n)");
    String s = stdin.readLineSync()!;
    if (checkGame(s)) {
      showNewGame();
    }
  }
  bool getEnd() {
    return this.end;
  }

  bool checkGame(String s) {
    if (s == "y" || s == "Y") {
      end = false;
      return true;
    } else if (s == "n" || s == "N") {
      return false;
    }
    return false;
  }

  int random(int min, int max) {
    return min + Random().nextInt(max - min);
  }

  void newMonster(String name) {
    m1.setName(name);
    //m1.setTypeC(typeC);
  }

  void showPlayer() {
    int lv = p1.getLv();
    int hp = p1.getHp();
    int mp = p1.getMp();
    int atk = p1.getAtk();
    int def = p1.getDef();
    int exp = p1.getExp();
    int str = p1.getStr();
    int vit = p1.getVit();
    int intel = p1.getIntel();
    int maxExp = p1.getMaxExp();
    int maxHp = p1.getMaxHp();
    int maxMp = p1.getMaxMp();
    print("------------------------------------------------");
    print("Name : " + p1.getName() + "     Job : " + p1.getNameJob());
    print("Stat !");
    print("HP : $hp / $maxHp");
    print("MP : $mp / $maxMp");
    print("Lv : $lv        Str : $str");
    print("Atk : $atk      Vit : $vit");
    print("Def : $def      Int : $intel");
    print("Exp : $exp / $maxExp ");
    print("------------------------------------------------");
  }

  void showMonster() {
    int lv = m1.getLv();
    int hp = m1.getHp();
    int atk = m1.getAtk();
    int def = m1.getDef();
    int exp = m1.getExp();
    print("------------------------------------------------");
    print("Name : " + m1.getName());
    print("Lv : $lv");
    print("HP : $hp");
    print("Atk : $atk");
    print("Def : $def");
    print("Exp : $exp");
    print("------------------------------------------------");
  }

  void attack() {
    win = false;
    while (!win) {
      if (p1.getRank() == 0) {
        var a; // a = attack
        print("");
        print("------------------------------------------------");
        print("x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
        print("Normal Attack : press 1");
        print("Lightly Healing : press 2");
        print("Which one ? : ");
        a = int.parse(stdin.readLineSync()!);
        print("x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
        if (a == 1) {
          m1.setHp(m1.getHp() - (p1.getAtk() - m1.getDef()));
          if (m1.getHp() <= 0) {
            int mExp = m1.getExp();
            showMadeDamage(m1.getName(), p1.getAtk());
            print("Monster was dead !!");
            print("You receive $mExp exp !!");
            p1.setExp(p1.getExp() + m1.getExp());
            currentExp();
            upLv();
            win = true;
          } else {
            showMadeDamage(m1.getName(), p1.getAtk());
            int hp = m1.getHp() + (p1.getAtk() - m1.getDef());
            int newHp = m1.getHp();
            print(m1.getName() + " HP $hp --> $newHp");
            monsterAttacking();
          }
        } else if (a == 2) {
          p1.setHp(p1.getHp() + (p1.getAtk() ~/ 2));
          print(p1.getAtk() ~/ 2);
          print(p1.getHp() + (p1.getAtk() ~/ 2));
          monsterAttacking();
        }
      } else if (p1.getRank() == 1) {
        class1skill();
      } else if (p1.getRank() == 2) {
        class2skill();
      }
    }
  }

  void class1skill() {
    var a; // a = attack
    print("");
    print("------------------------------------------------");
    print("x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
    print("Normal Attack : press 1");
    print("Lightly Healing : press 2");
    class1SkillCheckShow();
    print("Which one ? : ");
    a = int.parse(stdin.readLineSync()!);
    print("x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
    if (a == 1) {
      m1.setHp(m1.getHp() - (p1.getAtk() - m1.getDef()));
      if (m1.getHp() <= 0) {
        int mExp = m1.getExp();
        showMadeDamage(m1.getName(), class1SkillCheck());
        print("Monster was dead !!");
        print("You receive $mExp exp !!");
        p1.setExp(p1.getExp() + m1.getExp());
        currentExp();
        upLv();
        win = true;
      } else {
        showMadeDamage(m1.getName(), class1SkillCheck());
        int hp = m1.getHp() + (p1.getAtk() - m1.getDef());
        int newHp = m1.getHp();
        print(m1.getName() + " HP $hp --> $newHp");
        monsterAttacking();
      }
    } else if (a == 2) {
      p1.setHp(p1.getHp() + (p1.getAtk() ~/ 2));
      print(p1.getAtk() ~/ 2);
      print(p1.getHp() + (p1.getAtk() ~/ 2));
      monsterAttacking();
    } else if (a == 3) {
      if (p1.getMp() >= 10) {
        p1.setMp(p1.getMp() - 10);
        m1.setHp(m1.getHp() - (class1SkillCheck() - m1.getDef()));
        if (m1.getHp() <= 0) {
          int mExp = m1.getExp();
          showMadeDamage(m1.getName(), class1SkillCheck());
          print("Monster was dead !!");
          print("You receive $mExp exp !!");
          p1.setExp(p1.getExp() + m1.getExp());
          currentExp();
          upLv();
          win = true;
        } else {
          showMadeDamage(m1.getName(), class1SkillCheck());
          int hp = m1.getHp() + (class1SkillCheck() - m1.getDef());
          int newHp = m1.getHp();
          print(m1.getName() + " HP $hp --> $newHp");
          monsterAttacking();
        }
      } else {
        print("Not enough mana !! choose again !");
      }
    }
  }

  void class2skill() {
    var a; // a = attack
    print("");
    print("------------------------------------------------");
    print("x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
    print("Normal Attack : press 1");
    print("Lightly Healing : press 2");
    class1SkillCheckShow();
    class2SkillCheckShow();
    print("Which one ? : ");
    a = int.parse(stdin.readLineSync()!);
    print("x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
    if (a == 1) {
      m1.setHp(m1.getHp() - (p1.getAtk() - m1.getDef()));
      if (m1.getHp() <= 0) {
        int mExp = m1.getExp();
        showMadeDamage(m1.getName(), class1SkillCheck());
        print("Monster was dead !!");
        print("You receive $mExp exp !!");
        p1.setExp(p1.getExp() + m1.getExp());
        currentExp();
        upLv();
        win = true;
      } else {
        showMadeDamage(m1.getName(), class1SkillCheck());
        int hp = m1.getHp() + (p1.getAtk() - m1.getDef());
        int newHp = m1.getHp();
        print(m1.getName() + " HP $hp --> $newHp");
        monsterAttacking();
      }
    } else if (a == 2) {
      p1.setHp(p1.getHp() + (p1.getAtk() ~/ 2));
      print(p1.getAtk() ~/ 2);
      print(p1.getHp() + (p1.getAtk() ~/ 2));
      monsterAttacking();
    } else if (a == 3) {
      if (p1.getMp() >= 10) {
        p1.setMp(p1.getMp() - 10);
        m1.setHp(m1.getHp() - (class1SkillCheck() - m1.getDef()));
        if (m1.getHp() <= 0) {
          int mExp = m1.getExp();
          showMadeDamage(m1.getName(), class1SkillCheck());
          print("Monster was dead !!");
          print("You receive $mExp exp !!");
          p1.setExp(p1.getExp() + m1.getExp());
          currentExp();
          upLv();
          win = true;
        } else {
          showMadeDamage(m1.getName(), class1SkillCheck());
          int hp = m1.getHp() + (class1SkillCheck() - m1.getDef());
          int newHp = m1.getHp();
          print(m1.getName() + " HP $hp --> $newHp");
          monsterAttacking();
        }
      } else {
        print("Not enough mana !! choose again !");
      }
    } else if (a == 4) {
      if (p1.getMp() >= 30) {
        p1.setMp(p1.getMp() - 10);
        m1.setHp(m1.getHp() - (class2SkillCheck() - m1.getDef()));
        if (m1.getHp() <= 0) {
          int mExp = m1.getExp();
          showMadeDamage(m1.getName(), class2SkillCheck());
          print("Monster was dead !!");
          print("You receive $mExp exp !!");
          p1.setExp(p1.getExp() + m1.getExp());
          currentExp();
          upLv();
          win = true;
        } else {
          showMadeDamage(m1.getName(), class2SkillCheck());
          int hp = m1.getHp() + (class2SkillCheck() - m1.getDef());
          int newHp = m1.getHp();
          print(m1.getName() + " HP $hp --> $newHp");
          monsterAttacking();
        }
      } else {
        print("Not enough mana !! choose again !");
      }
    }
  }

  void class1SkillCheckShow() {
    p1.checkJob1Show();
  }

  int class1SkillCheck() {
    return p1.checkJob1();
  }

  void class2SkillCheckShow() {
    p1.checkJob2Show();
  }

  int class2SkillCheck() {
    return p1.checkJob2();
  }

  void monsterAttacking() {
    print("");
    print("m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-");
    print("Monster is attacking !!     ");
    if (m1.getAtk() <= p1.getDef()) {
      print("You block success!          ");
      print("You Take 0 damage !!        ");
      print("Monster too weak !          ");
      print("m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-");
    } else {
      p1.setHp(p1.getHp() - (m1.getAtk() - p1.getDef()));
      int damage = m1.getAtk() - p1.getDef();
      print("You Take $damage damage !!      ");
      if (p1.getHp() <= 0) {
        print("You dead. . .               ");
        print("m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-");
        win = true;
        end = true;
        return;
      } else {
        int hp = p1.getHp() + (m1.getAtk() - p1.getDef());
        int newHp = p1.getHp();
        print("Your HP $hp --> $newHp         ");
        print("m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-m๋-");
      }
    }
  }

  void showMadeDamage(String name, int atk) {
    int damage = atk - m1.getDef();
    print("You made $damage damage to " + name + " !!");
  }

  void currentExp() {
    int exp = p1.getExp();
    int maxExp = p1.getMaxExp();
    print("currentExp : $exp / $maxExp !");
  }

  void increaseExp() {
    if (win) {
      p1.setExp(p1.getExp() + m1.getExp());
      upLv();
    }
  }

  void upLv() {
    if (p1.getExp() >= p1.getMaxExp()) {
      p1.setLv(p1.getLv() + 1);
      p1.setExp(p1.getExp() - p1.getMaxExp());
      p1.setMaxExp(p1.getLv() * 50);
      int lvN = p1.getLv();
      int lv1 = p1.getLv() - 1;
      print("LV up !!");
      print("$lv1 --> $lvN");
      upStat();
      p1.class1change(p1.getLv());
      p1.class2change(p1.getLv());
    }
  }

  void upStat() {
    print("Your status can upgrade 3 time !!");
    for (int i = 1; i < 4; i++) {
      print("Time $i !");
      print("upgrade str : press 1");
      print("upgrade vit : press 2");
      print("upgrade int : press 3");
      int up = int.parse(stdin.readLineSync()!);
      checkUpStat(up);
      p1.updateStat(up);
      print("Upgrade Success !!");
    }
    showPlayer();
  }

  void checkUpStat(int up) {
    switch (up) {
      case 1:
        p1.setStr(p1.getStr() + 1);
        break;
      case 2:
        p1.setVit(p1.getVit() + 1);
        break;
      case 3:
        p1.setIntel(p1.getIntel() + 1);
        break;
    }
  }

  void walk() {
    int r = random(0, 100);
    if (r <= 50) {
      freeExp();
    } else {
      faceMonster();
    }
  }

  void freeExp() {
    p1.setExp(p1.getExp() + 10);
    print("clearly way !! You receive 10 exp!");
    currentExp();
    upLv();
  }

  void backHome() {
    int oldHp = p1.getHp();
    int oldMp = p1.getMp();
    int newHp = p1.getMaxHp();
    int newMp = p1.getMaxMp();
    print("Ahh I will return to be the KING !!");
    print("*get Heal HP and MP*");
    print("HP : $oldHp --> $newHp");
    print("MP : $oldMp --> $newMp");
    p1.setMp(p1.getMaxMp());
    p1.setHp(p1.getMaxHp());
    showPlayer();
  }

  void faceMonster() {
    m1.setName(randomTypeMonster());
    m1.setStatMonster(p1.getLv());
    showMonster();
    attack();
  }

  String randomTypeMonster() {
    int r = random(1, 5);
    var tn; //tn = type name
    switch (r) {
      case 1:
        tn = "Slime";
        break;
      case 2:
        tn = "Bridy";
        break;
      case 3:
        tn = "Sneaky";
        break;
      case 4:
        tn = "Insecter";
        break;
      case 5:
        tn = "Wile Wolf";
        break;
    }
    return tn;
  }

  void showNewGame() {
    print("What is your name ? : ");
    String newN = stdin.readLineSync()!;
    p1.setName(newN);
    p1.defaultStat();
    showPlayer();
  }

  void showGame() {
    print("-----------------------------------------------");
    print("| You want to walk ?                : press 1  |");
    print("| You want to back home ?           : press 2  |");
    print("| You want yo check your status ?   : press 0  |");
    print("| You want to End this adventure?   : prees -1 |");
    print("-----------------------------------------------");
    int g = int.parse(stdin.readLineSync()!);
    if (g == 1) {
      walk();
    } else if (g == 2) {
      backHome();
    } else if (g == 0) {
      showPlayer();
      end = false;
    } else if (g == -1) {
      print("This You !!");
      showPlayer();
      end = true;
      return;
    }
  }
}
