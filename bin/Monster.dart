import 'dart:math';
import 'Character.dart';

class Monster extends Character {
  Monster(super.name) {
    defaultStat();
  }
  int random(int min, int max) {
    return min + Random().nextInt(max - min);
  }

  void defaultStat() {
    setLv(1);
    setExp(5);
    setAtk(30);
    setDef(5);
    setHp(5);
  }

  void setStatMonster(int lv) {
    setLv(random(lv - 2, lv));
    setAtk(random(lv - 2, lv) * random(4, 7));
    setDef(random(lv - 2, lv) * random(1, 3));
    setHp(random(lv - 2, lv) * random(20, 25));
    setExp(atk + def + (hp));
    if (lv <= 2) {
      setLv(1);
      setAtk(1 * random(4, 7));
      setDef(1 * random(1, 3));
      setHp(1 * random(20, 25));
      setExp(atk + def + (hp));
    }
  }
}
