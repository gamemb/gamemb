import 'dart:io';
import 'dart:math';

import 'Character.dart';
import 'Job.dart';

class Player extends Character {
  var gender;
  var mp;
  var str;
  var vit;
  var intel;
  var maxExp;
  var maxHp;
  var maxMp;
  Job j1 = new Job("Adventure");
  String getNameJob() {
    return j1.getNameJob();
  }

  void setNameJob(String nameJob) {
    j1.setNameJob(nameJob);
  }

  int checkJob1() {
    int incRate = j1.checkJob1();
    return getAtk() * incRate;
  }

  void checkJob1Show() {
    j1.checkJob1Show();
  }

  int checkJob2() {
    int incRate = j1.checkJob2();
    return getAtk() * incRate;
  }

  void checkJob2Show() {
    j1.checkJob2Show();
  }

  Player(super.name) {}
  String getGender() {
    return this.gender;
  }

  int getMp() {
    return this.mp;
  }

  int getMaxMp() {
    return this.maxMp;
  }

  int getStr() {
    return this.str;
  }

  int getVit() {
    return this.vit;
  }

  int getIntel() {
    return this.intel;
  }

  int getMaxExp() {
    return this.maxExp;
  }

  int getMaxHp() {
    return this.maxHp;
  }

  void setGender(String gender) {
    this.gender = gender;
  }

  void setMp(int mp) {
    this.mp = mp;
  }

  void setMaxMp(int maxMp) {
    this.maxMp = maxMp;
  }

  void setStr(int str) {
    this.str = str;
  }

  void setVit(int vit) {
    this.vit = vit;
  }

  void setIntel(int intel) {
    this.intel = intel;
  }

  void setMaxExp(int maxExp) {
    this.maxExp = maxExp;
  }

  void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  int getRank() {
    return j1.getRank();
  }

  void defaultStat() {
    setStr(random(1, 5));
    setVit(random(1, 5));
    setIntel(random(1, 5));
    setLv(1);
    setNameJob("Adventure");
    setExp(0);
    setMaxExp(getLv() * 50);
    setAtk(30);
    setDef(15);
    setMaxHp(200);
    setMaxMp(50);
    setHp(200);
    setMp(50);
    updateStatDefault();
  }

  void increaseAtkDefault(int str) {
    atk = 30 + (str * 3);
  }

  void increaseDefDefault(int vit) {
    def = 15 + (vit * 2);
  }

  void increaseHpDefault(int vit) {
    maxHp = 200 + (vit * 20);
    hp = 200 + (vit * 20);
  }

  void increaseMpDefault(int intel) {
    maxMp = 50 + (intel * 12);
    mp = 50 + (intel * 12);
  }

  ////////////////////////////////////////////////////////////////////////
  void increaseAtk(int str) {
    str++;
    atk += 3;
  }

  void increaseDef(int vit) {
    vit++;
    def += 2;
  }

  void increaseMaxHp(int vit) {
    vit++;
    maxHp += 20;
  }

  void increaseMaxMp(int intel) {
    intel++;
    maxMp += 12;
  }

  void updateStatDefault() {
    increaseAtkDefault(str);
    increaseDefDefault(vit);
    increaseHpDefault(vit);
    increaseMpDefault(intel);
  }

  void updateStat(int up) {
    switch (up) {
      case 1:
        increaseAtk(str);
        break;
      case 2:
        increaseDef(vit);
        increaseMaxHp(vit);
        break;
      case 3:
        increaseMaxMp(intel);
        break;
    }
  }

  void class1change(int lv) {
    j1.class1(lv);
    setNameJob(j1.getNameJob());
  }

  void class2change(int lv) {
    j1.class2(lv);
    setNameJob(j1.getNameJob());
  }
}
