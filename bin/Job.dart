import 'dart:io';

import 'Archer.dart';
import 'Mage.dart';
import 'Sword_man.dart';
import 'Thief.dart';

class Job implements Sword_man, Mage, Archer, Thief {
  var rank = 0;
  var nameJob;
  Job(String nameJob) {
    this.nameJob = nameJob;
  }
  String getNameJob() {
    return this.nameJob;
  }

  int getRank() {
    return this.rank;
  }

  void setNameJob(String nameJob) {
    this.nameJob = nameJob;
  }

  void class1(int lv) {
    if (nameJob == "Adventure" && lv == 10) {
      showClass1();
    }
  }

  void class2(int lv) {
    if (lv == 11) {
      showClass2(getNameJob());
      changeJob2(getNameJob());
    }
  }

  void changeJob2(String c2) {
    rank = 2;
    switch (c2) {
      case "Sword man":
        setNameJob("Knight");
        break;
      case "Mage":
        setNameJob("Wizard");
        break;
      case "Archer":
        setNameJob("Hunter");
        break;
      case "Thief":
        setNameJob("Thief");
        break;
    }
  }

  void showClass2(String c2) {
    print("Congratolation !! You reach to Lv 15 !!");
    switch (c2) {
      case "Sword man":
        print("Your job upgrade to Knight !!");
        break;
      case "Mage":
        print("Your job upgrade to Wizard !!");
        break;
      case "Archer":
        print("Your job upgrade to Hunter !!");
        break;
      case "Thief":
        print("Your job upgrade to Thief !!");
        break;
    }
  }

  void showClass1() {
    rank = 1;
    print("Congratolation !! You reach to Lv 10 !!");
    print("You can change your job (Class 1) !!");
    print("Want to be the Sword man   : Press 1 ");
    print("Want to be the Mage        : Press 2 ");
    print("Want to be the Archer      : Press 3 ");
    print("Want to be the Thief       : Press 4 ");
    int c1 = int.parse(stdin.readLineSync()!);
    changeJob1(c1);
  }

  void changeJob1(int c1) {
    switch (c1) {
      case 1:
        setNameJob("Sword man");
        break;
      case 2:
        setNameJob("Mage");
        break;
      case 3:
        setNameJob("Archer");
        break;
      case 4:
        setNameJob("Thief");
        break;
    }
  }

  int checkJob1() {
    switch (getNameJob()) {
      case "Sword man":
        return sword_bash();
        break;
      case "Mage":
        return fire_ball();
        break;
      case "Archer":
        return double_staff();
        break;
      case "Thief":
        return double_attack();
        break;
    }
    return 1;
  }

  int checkJob2() {
    switch (getNameJob()) {
      case "Knight":
        return double_bash();
        break;
      case "Wizard":
        return fire_meteo();
        break;
      case "Hunter":
        return blast_shot();
        break;
      case "Assasin":
        return sonic_blow();
        break;
    }
    return 1;
  }

  void checkJob2Show() {
    switch (getNameJob()) {
      case "Knight":
        print("Double bash : press 4");
        break;
      case "Wizard":
        print("Fire meteo : press 4");
        break;
      case "Hunter":
        print("Blast shot : press 4");
        break;
      case "Assasin":
        print("Sonic blow : press 4");
        break;
    }
  }

  void checkJob1Show() {
    if (rank == 1) {
      switch (getNameJob()) {
        case "Sword man":
          print("Sword bash : press 3");
          break;
        case "Mage":
          print("Fire ball : press 3");
          break;
        case "Archer":
          print("double_staff : press 3");
          break;
        case "Thief":
          print("double_attack : press 3");
          break;
      }
    } else if (rank == 2) {
      switch (getNameJob()) {
        case "Knight":
          print("Sword bash : press 3");
          break;
        case "Wizard":
          print("Fire ball : press 3");
          break;
        case "Hunter":
          print("double_staff : press 3");
          break;
        case "Assasin":
          print("double_attack : press 3");
          break;
      }
    }
  }

  @override
  int sword_bash() {
    return 2;
  }

  @override
  int fire_ball() {
    return 2;
  }

  @override
  int double_attack() {
    return 2;
  }

  @override
  int double_staff() {
    return 2;
  }

  @override
  int blast_shot() {
    return 4;
  }

  @override
  int double_bash() {
    return 4;
  }

  @override
  int fire_meteo() {
    return 4;
  }

  @override
  int sonic_blow() {
    return 4;
  }
}
