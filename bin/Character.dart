import 'dart:io';
import 'dart:math';

abstract class Character {
  var name;

  var atk;
  var def;
  var lv;
  var hp;
  var exp;
  Character(String name) {
    this.name = name;
  }
  String getName() {
    return this.name;
  }

  int getAtk() {
    return this.atk;
  }

  int getDef() {
    return this.def;
  }

  int getLv() {
    return this.lv;
  }

  int getHp() {
    return this.hp;
  }

  int getExp() {
    return this.exp;
  }

  void setAtk(int atk) {
    this.atk = atk;
  }

  void setName(String name) {
    this.name = name;
  }

  //void setTypeC(String type) {}

  void setDef(int def) {
    this.def = def;
  }

  void setLv(int lv) {
    this.lv = lv;
  }

  void setHp(int hp) {
    this.hp = hp;
  }

  void setExp(int exp) {
    this.exp = exp;
  }

  int random(int min, int max) {
    return min + Random().nextInt(max - min);
  }
}
